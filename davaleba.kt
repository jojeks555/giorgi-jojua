import java.io.PipedReader

fun main() {
    var g1 = Fraction(3.0, 6.0)
    var g2 = Fraction(7.0, 8.0)

    println(g1 == g2)

    println(g1)
    println(g2)
    println()
    println("Shekvecili : ${g1.shekveca()}")
    println("Shekvecili : ${g2.shekveca()}")
    println()
    println("Jami : ${add(g1,g2)}")
    println("Namravli ${Gamravleba(g1,g2)}")
}

class Fraction(n: Double, d: Double) {
    public var numerator: Double = n
    public var denominator: Double = d
    fun pirveli(): Double {
        return numerator
    }

    fun meore(): Double {
        return denominator
    }
    override fun equals(other: Any?): Boolean {
        if (other is Fraction) {
            if (numerator + other.denominator / denominator == other.numerator) {
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "$numerator / $denominator"

    }
    fun shekveca(): String {
        var i: Double = 1.0
        while (i < denominator) {
            i = i + 1;
            if (denominator % i == 0.0 && numerator % i == 0.0) {
                numerator = numerator / i
                denominator = denominator / i
                i=1.0;
            }
        }
        return "$numerator / $denominator"
    }
}
fun add( g1: Fraction, g2: Fraction):String{
    var First: Fraction = g1
    var Second: Fraction = g2
    var i: Double = Pirv.meore()
    while (i <= First.meore()*Second.meore()){
        if (i % First.meore() == 0.0 && i%Second.meore() == 0.0)
            break
        else i=i+First.meore()
    }
    return "${First.pirveli()*(i/First.meore())+Second.pirveli()*(i/Second.meore())} / $i"
}
fun Gamravleba( g1: Fraction, g2: Fraction): String{
    var First: Fraction = g1
    var Second: Fraction = g2
    return "${First.pirveli() * Second.pirveli()} / ${Second.pirveli()*Second.meore()}"
}